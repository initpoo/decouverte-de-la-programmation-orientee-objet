# Bienvenue sur ce site consacré à la découverte de la POO

Contenu du site :

1. Activité de découverte : un élevage de lapin au XIIème siècle
2. Présentation de la Programmation Orientée Objet (POO)
3. Activité : résolution du problème des lapins
4. Constructeur et accesseurs en lecture ou en écriture
5. Activité : une classe Fraction
6. Attributs et méthodes publics ou privés