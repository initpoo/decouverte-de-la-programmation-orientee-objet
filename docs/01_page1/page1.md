# Evolution d'une population de lapins

## Présentation du problème :

*On possède un couple de lapins qui vient de naître, combien possèderons-nous de couples de lapins après un nombre de mois donné si chaque couple engendre chaque mois un nouveau couple à compter du début du troisième mois de son existence ?*

![beaux petits lapins](lapins.jpg)

## Questions :

Pour plus de clarté et de rigueur :

1. on appellera "date 0" le jour du début de l'expérience, qui est aussi le jour de naissance du premier couple de lapins.
2. on appellera "date 1" le premier jour du second mois de l'expérience.
3. on appellera "date 2" le premier jour du troisième mois de l'expérience. Et ainsi de suite...

De plus, on supposera (ce qui était sous-entendu dans l'énoncé), que tous les couples en âge de se reproduire se reproduisent effectivement, qu'aucun lapin ne meure ni ne s'échappe.

**1)** A quelle date notre premier couple se reproduira-t-il pour la première fois ?

{{ multi_qcm(
    [
    "Réponse à la question n°1 ?",
    ["date 0", "date 1", "date 2","date 3"],
    [3]
    ],
    qcm_title = "Question n°1",
    multi = False,
    DEBUG = False,
    shuffle = True,
    hide = True
) }}

**2)** Combien aurons-nous de couples de lapins aux dates 2, 3 et 4 ?

{{ multi_qcm(
    [
    "Réponse à la question n°2 ?",
    ["2, 2 et 3", "2, 3 et 3", "2, 3 et 4", "2, 3 et 5"],
    [4]
    ],
    qcm_title = "Question n°2",
    multi = False,
    DEBUG = False,
    shuffle = True,
    hide = True
) }}

**3)** Combien aurons-nous de couples de lapins à la date 20 ?